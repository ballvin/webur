Webur Vagrant Installation
======================================

Requrements
-----------
Install these first:

* Virtualbox
* Vagrant >= 1.7.0
* Git

Recommended
-----------
* Sequel Pro (MySQL client on OSX)


Installation
------------
* git clone https://ballvin@bitbucket.org/ballvin/webur.git
* vagrant up

Setup	
-----
	
VM is created at 192.168.44.44


