#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD='passwd'
PROJECTFOLDER='www'

# create project folder
# sudo mkdir "/var/www/html/${PROJECTFOLDER}"

# update / upgrade
sudo apt-get update
sudo apt-get -y upgrade

# install apache 2.5 and php 5.5
sudo apt-get install -y apache2
sudo apt-get install -y php5


# setup hosts file
VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot "/var/www/html/${PROJECTFOLDER}"
    <Directory "/var/www/html/${PROJECTFOLDER}">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-available/000-default.conf

# enable mod_rewrite
sudo a2enmod rewrite


# set some php values
echo 'echo "short_open_tag = On" >> /etc/php5/apache2/conf.d/user.ini' | sudo -s
echo 'echo "asp_tags = On" >> /etc/php5/apache2/conf.d/user.ini' | sudo -s
echo 'echo "display_errors = On" >> /etc/php5/apache2/conf.d/user.ini' | sudo -s
echo 'echo "error_reporting = E_ALL | E_STRICT" >> /etc/php5/apache2/conf.d/user.ini' | sudo -s
echo 'echo "error_log = /var/www/html/logs/php_errors.log" >> /etc/php5/apache2/conf.d/user.ini' | sudo -s
echo 'touch /var/www/html/logs/php_errors.log' | sudo -s
echo 'chmod 666 /var/www/html/logs/php_errors.log' | sudo -s



# restart apache
service apache2 restart

# install git
sudo apt-get -y install git


# cleanup unneeded file that was created during install
if [ -f '/var/www/html/index.html' ]; then
	sudo rm /var/www/html/index.html
fi	





